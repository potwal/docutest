# docutest

See [gitlab handbook](https://about.gitlab.com/handbook/markdown-guide/#diagrams) for more info.

## mermaid

See [mermaid](https://docs.gitlab.com/ee/user/markdown.html#mermaid) docu.

### graph

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```




## plantuml

include in md inline 

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```

or as a seperate file via include like `!include plantuml.puml`

```plantuml
!include ./plantuml.puml
```
